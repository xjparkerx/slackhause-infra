import salt.client
import pprint as pp

client = salt.client.LocalClient()
ret = client.cmd('*', 'test.ping', full_return=True)

pp.pprint(ret)