{% for service in 'iptables','ufw' %}
{{ service }}:
  service.dead:
    - name: {{ service }}
    - enable: False
{% endfor %}