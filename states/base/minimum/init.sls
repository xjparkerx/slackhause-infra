include:
  - users
  - standard-packages
  - clock-config
  - firewall

salt-minion:
  service.running:
  - enable: True
  - restart: True
  - watch:
    - file: /etc/salt/minion

/etc/salt/minion:
  file.managed:
    - source: salt://minimum/minion
    - user: root
    - group: root
    - mode: 644
    - physical_location: {{ pillar['physical_location'] }}
    - saltmaster_fqdn: {{ pillar['saltmaster_fqdn'] }}
    - datacenter: {{ pillar['Datacenter'] }}
    - require:
      - sls: standard-packages
    - template: jinja

America/New_York:
  timezone.system:
    - utc: False