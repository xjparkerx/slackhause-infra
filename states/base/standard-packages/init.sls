standard.packages:
  pkg.installed:
    - pkgs:
      - git
      - bash
      - salt-minion
      - tmux
      {% if grains['os'] == 'Alpine' %}
      - doas-sudo-shim
      {% endif %}