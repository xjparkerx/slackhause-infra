add users:
  user.present:
    - name: jparker
    - fullname: Justin
    - shell: /bin/bash
    - home: /home/jparker
    - groups:
      {% if grains['os'] == 'Alpine' %}
      - wheel
      {% elif grains['os'] == 'Ubuntu' %}
      - sudo
      {% endif %}

create ssh directory:
  file.directory:
    - name: /home/jparker/.ssh
    - user: jparker
    - mkdirs: True

push keys file:
  file.managed:
    - name: /home/jparker/.ssh/authorized_keys
    - user: jparker
    - mode: 644
    - require:
      - file: /home/jparker/.ssh
    - source: salt://users/authorized_keys

remove users:
  user.absent:
    - name: cloudinit
    - purge: True
    - force: False
