{% if grains['os'] == 'Ubuntu' %}
{% set file_name = '/etc/openntpd/ntpd.conf' %}
{% if grains['os'] == 'OEL' %}
{% set file_name = '/etc/openntpd/ntpd.conf' %}
{% else %}
{% set file_name = '/etc/ntpd.conf' %}
{% endif %}

clock service:
  service.running:
    - name: openntpd
    - enable: True
    - watch:
      - file: {{ file_name }}

configure clock:
  pkg.installed:
    - name: openntpd

  file.managed:
    - name: {{ file_name }}
    - source: salt://clock-config/ntpd.conf
    - user: root
    - mode: 644
    - template: jinja
    - ntp_pool: {{ pillar['address_pool'] }}
    - require:
      - pkg: openntpd