tstminion1:
  healthcheck_config:
    Schema: http
    Port: 8080
    HealthOnStartup: true (startup)
    HealthFileDirectory: E:\\Atlas\\HealthCheckWrapper
    HealthFileName: health_status
    TargetHealthCheckEndpointCheckInterval: 5
    TargetHealthCheckEndpoints:
      - Schema: http
        HostName: cqvwbfvm001
        Port: 80
        Endpoint: aom/healthcheck
        ResponseFromContent: true
        ExpectedResponse: true
        SiteName: apiqana11.clm.docusign.net
      - Schema: http
        HostName: cqvwbfvw001
        Port: 80
        Endpoint: sitewarmup/warmup
        ResponseFromContent: false
        ExpectedResponse: 200
        SiteName: apidownloadqana11.springcm.com