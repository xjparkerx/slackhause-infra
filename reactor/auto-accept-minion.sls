{% if 'act' in data and data['act'] == 'pend' and data['id'].endswith('.slacknet.lan') %}
minion_add:
  wheel.key.accept:
    - args:
      - match: {{ data['id'] }}
{% endif %}